---
layout: post
title: "9. 5. 무순위 청약! 천안 부성지구 한라비발디 줍줍! 부성역 역세권..."
toc: true
---


## 천안 부성지구 한라비발디 무순위 줍줍 청약일정 평면도 분양가 중도금대출 입지분석

### 목차
 ◎ 천안 부성지구 한라비발디 무순위 청약일정

 ◎ 천안 부성지구 한라비발디 무순위 공급규모

 ◎ 천안 부성지구 한라비발디 무순위세대 평면도

 ◎ 천안 부성지구 한라비발디 무순위세대 분양가
 ◎ 천안 부성지구 한라비발디 무순위 청약사항
 ◎ 모집공고/분양홈페이지

 안녕하세요. 청정남입니다. 오늘은 충청남도 천안시 서북구 부대동 760번지, 부성지구 B-1BL에 공급되는 "천안 부성지구 한라비발디" 무순위 줍줍 입주자모집공고 및 청약일정 일일이 살펴보겠습니다. 천안 부성지구 한라비발디는 아파트 지하 2층, 뭍 25층, 9개 동, 총 654세대 규모로 조성되며 유익 중간 정당계약 및 예비입주자 약속 이강 잔여물량 214세대에 대한 무순위 청약을 진행하며 입주시기는 2025년 2월 예정입니다.
 

 천안 부성지구 한라비발디는 2026년 준공 예정인 수도권전철 1호선 부성역(가칭)이 애오라지 곧이어 앞에 들어서는 역세권 입지로 부성역은 직산역과 두정역 길거리 부성지구에 들어섭니다. 여기에 직산-부성간 1번 국도 확장 사입이 선년 준공되었고, 직산사거리 입체화 사업도 흥성 중이며, 서북-성거 국도 도대체 우회도로가 2024년 3월 개통을 앞두고 있습니다. 또한, 평택-오송간 KTX노선 2복선화 영업 및 공주-천안간 민자고속도로 사업도 예정되어 있어 다양한 교통호재를 갖추고 있습니다. 부성지구 내에 초, 중, 고등학교가 들어설 예정으로 근방 북일고, 북일여고, 공주대 등이 가까운 교육환경을 갖추고 있네요. 같이 삼성디스플레이아산캠퍼스, 아산디스플레이시티, 천안산업단지 등이 단지 반경 10km 내에 자리 잡고 있어 직주근접 배후수요까지 풍부할 전망입니다.
 

 지난 7월 청약진행한 천안 부성지구 한라비발디는 1순위 관련 고장 최상 2.74대 1이라는 청약 결과를 내었고, 최고가점 63점, 평균가점 30점 대의 분포를 보이며 그대 결과가 이번에 214세대라는 무순위 물량으로 나왔다고 보이네요. 지구가 조성되면 성성지구와 비슷한 활약 인프라를 갖출 것으로 기대되며 성성지구 눈치 "성성레이크타운푸르지오 1, 2, 3차" 전용 84타입이 이승 6억 초반에서 실거래되고 있어 향후 부성지구도 일정한 갭을 두고 시세를 따라갈 것으로 보이네요. 두정역 인근으로는 2017년 2월 준공한 "e편한세상두정3차" 전용 84타입이 4억 7,000만 등불 선에서 거래되고 있어 연식 등을 감안했을 공양 약 1억 기망 [천안 롯데캐슬 더 두정](http://lotte-castle.com) 내외의 안전마진 예상됩니다.
 

 실수요자들이 선호하는 전용 84타입으로 구성되어 있으며 개발구역으로 향후 미래가치는 풍부하나 한라비발디 분양결과는 부동산 조정지역 지정이 해제되지 않은 상황에서 분양이 진행돼 향후 부동산 시장의 방향을 가늠하는 잣대로 작용할 것으로 보이며 현금 부동산 시장의 흐름이 좋지 않아 무순위 줍줍이 214세대나 나온 것에 대해 고려할 필요는 있겠습니다. 고작 부성역 신설, 성성지구 연결도로 설시 등 좋은 입지조건이 긍정적인 요인으로 작용할 것으로 보이며 천안시에 거주하는 무주택세대구성원은 작정 가져 보시길 바랍니다. 나란히 재당첨제한은 당첨일로부터 7년, 전매제한은 소유권이전등기일까지, 중도금대출은 이자후불제 적용돼 50% 범위에서 가능합니다.

 부성역세권 새도시
 계제 약 5분 감 지하철 1호선 부성역(신설, 가칭)
 두정, 성성지구를 이은 천안 북부권 개바르이 하이라이트 부성지구
 

 도보거리 교육환경
 내심 놓이는 겨우 내 국공립 어린이집(예정)
 부대초, 부성중, 신당고, 공주대, 한국기술교육대(제2캠퍼스)
 

 상쾌한 파크라이프
 가까운 성성호수공원으로 여유로운 힐링 생활
 휴양여가시설, 자연경관보전시설, 생태학습체험시설 등 조성
 

 완벽한 생활인프라
 삼성SDI, 천안산업단지, 백석농공단지 등 직주근접
 코스트코, 롯데마트, 메가박스 인접으로 풍요로운 생활

 

### 천안 부성지구 한라비발디 무순위 청약일정

 무순위 사후접수 : 22. 9. 5. (월)
 당첨자 표발 : 22. 9. 13. (화)
 당첨자 서류제출 : 22. 9. 16. (금) ~ 22. 9. 19. (월)
 계약체결 : 22. 9. 20. (화) ~ 22. 9. 21. (수)
 청약방법 : 한국부동산원 청약홈
 

### 천안 부성지구 한라비발디 무순위 공급규모
 공급위치 : 충청남도 천안시 서북구 부대동 760번지 (부성지구 B-1BL)
 공급규모 : 아파트 지하 2층, 평생 25층, 9개 동, 총 654세대 정당계약 및 예비입주자 내락 이후 잔여물량 214세대
 입주시기 : 2025년 2월 예정

 

### 천안 부성지구 한라비발디 무순위세대 평면도

 

### 천안 부성지구 한라비발디 무순위세대 분양가
 84A타입 : 4.23억
 84B타입 : 4.22억
 84C타입 : 4.27억

 천안 부성지구 한라비발디 무순위 줍줍 분양가 납부는 계약금 10%, 중도금 60%, 잔금 30%로 납부가능하며, 발코니 확장 공사비는 계약금 10%, 중도금 10%, 잔금 80%로 납부가능합니다.

 

### 천안 부성지구 한라비발디 무순위 청약사항
 ◎ 지역구분
 비투기과열지구 및 청약과열지역, 분양가상한제 미적용 민영주택
 ◎ 공급대상
 천안시에 거주하는 무주택세대구성원인 성년자
 단, 외국인은 청약불가
 청약통장 가입여부 무관 및 청약신청금 없음
 ◎ 청약신청 제한
 - 동정남 주택에 당첨되어 계약을 체결한 문 혹은 예비입주자 속 추가입주자로 선정된 자
 - 경황 주택에 당첨된 추후 계약을 체결하지 않은 자
 - 부적격 당첨자로서 네놈 때 중에 있는 자
 - 석일 재당첨제한 목적 주택에 당첨되어 이적 댁네 가운데 중에 있는 자
 ◎ 당첨자 선정방법
 추첨의 방식으로 무작위 선정
 ◎ 재당첨제한 : 당첨일로부터 7년
 ◎ 전매제한 : 소유권이전등기일까지
 ◎ 실거주의무 : 없음
 ◎ 중도금대출 : 이자후불제, 50%
 

### 모집공고/분양홈페이지

 천안 부성지구 한라비발디 무순위 줍줍 청약관련하여 계약시 유의사항 등은 입주자모집공고문 확인 바라며 사이버모델하우스 등은 아래편 공식 홈페이지 참고 바랍니다.
 

### 채널안내/유의사항
 기수 가권 마련을 위한 첫단계! 부동산 청약은 당첨확률이 희박하다?, 어렵고 복잡하다? 조금만 관심가지면 여러분들도 당첨될 복운 있습니다.

 부동산청약, 줍줍청약요약, 부동산리뷰 등 다양한 재료 확인하고 청약에 당첨되시길 진심으로 바랍니다.

 청정남(청약정리하는남자) 유튜브
 리치몬드 경제이야기
 ※ 부동산청약 및 투자 등에 관련하여 핵심적인 사항, 개인적인 의견을 작성한 것이니 청약, 투자 등의 책에 있어서 투자자 본인에게 있음을 알려드립니다. 입주자모집공고 및 분양홈페이지를 통해 작성한 것이니 최후 수정사항 있으면 참고하고 수정하도록 하겠습니다.
